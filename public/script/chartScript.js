

// $.get("/getGraphData",function(data){
//   //  console.log(data);
//   console.log(data);
//    graph(data);
// });
 $.ajax({
    type:"get",
    url:"/getGraphData",
    success:function(data){
        console.log(data);
        graph(data);
    }
        ,error:alert("something went wrong")
    });
     var graph =function graph(data){
       console.log(data);
    
      Highcharts.chart('container', {
        chart: {
            type: 'bar',
            marginLeft: 150
        },
        title: {
            text: 'extra run giveen by each team in 2016'
        },
        subtitle: {
            text: 'Source: <a href="https://highcharts.uservoice.com/forums/55896-highcharts-javascript-api">bcci.tv</a>'
        },
        xAxis: {
            type: 'category',
            title: {
                text: null
            },
            min: 0,
            max: 8,
            scrollbar: {
                enabled: false
            },
            tickLength: 0
        },
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: "runs given by the team",
                align: 'high'
            }
        },
      
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'matchplyed',
            data: data
                
            
        }]});
    
}
