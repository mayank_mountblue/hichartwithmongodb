var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var app = express();

var Url = "mongodb://localhost:27017/ipldata";



mongoose.connect(Url).then(success => {
	console.log("DB is Connected");
}).catch(err => {
	console.log("DB is not Connected");

});
var schema = new mongoose.Schema({}, {
	strict: false
});
var match = mongoose.model('match', schema);

var match_per_season = [];
match.aggregate([

	{
	$group: {
		_id: '$season',
		count: {
			$sum: 1,
		
		}
	}
}]).exec(function (err, result) {
	if (err) console.log('got error while facthing data');
	else {
		console.log(result);
		result.forEach(function (element) {
			var match_count_per_season = [];
			match_count_per_season.push(element._id.toString());
			match_count_per_season.push(element.count);
			match_per_season.push(match_count_per_season);
		});

	}
	console.log(match_per_season);
});

router.get("/", function (req, res) {
	res.send(match_per_season);
});




module.exports = {

	router: router
}