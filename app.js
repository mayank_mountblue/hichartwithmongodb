var express = require("express");
var app = express();
var bp = require("body-parser");
var path = require("path");
var fs =require("fs");
var csvjson = require('csvjson');
var $ = require('jquery'); 
app.use(express.static("public"));
var extra_run_exceded_by_bowler=require("./matches_per_season");
var GraphData =extra_run_exceded_by_bowler.router;

app.use(bp.urlencoded({ extended: false }))
 
//var team_won_per_season =require("./filterData/team-won-per-season.js");
 app.use("/getGraphData",GraphData);

app.listen(3000);